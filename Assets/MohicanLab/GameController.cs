﻿// Author:
//       DEaDA <deada@deada.net>
// Copyright (c) 2016 Konstantin Arbuzov
using UnityEngine;
using System.Collections;
using DigitalRuby.SoundManagerNamespace;

[System.Serializable]
public class GameController : MonoBehaviour
{
	//	[SerializeField]
	//	public AudioSource[] soundAudioSources;
	//	[SerializeField]
	//	public AudioSource[] musicAudioSources;

	public GameModel model;
	SoundManagerDemo sounds;

	public void Awake ()
	{
		sounds = GetComponent<SoundManagerDemo> ();
	}

	public void Init (GameModel model)
	{
		this.model = model;

		GameEvent.onPickupObject += ItemPickup;
		GameEvent.onScoreUpdate += ScoreUpdate;
		GameEvent.onPlayerDied += GameOver;
		GameEvent.onGameReset += GameReset;
		GameEvent.onOpenCage += PlayMusic;
		GameEvent.onUseCar += PlayMusic;
		GameEvent.onPlayerShakeDoor += onPlayerShakeDoor;
		GameEvent.onPlayerDied += onPlayerCollide;
		GameEvent.onPlayerMove+= onPlayerMove;
	}


	void onPlayerMove ()
	{
		sounds.PlaySound (2);
	}

	void onPlayerCollide ()
	{
		sounds.PlaySound (1);
	}

	void onPlayerShakeDoor ()
	{
	}

	void PlayMusic ()
	{
		sounds.PlaySound (0);
		Invoke ("StartMusic", 0.5f);
	}

	void StartMusic ()
	{
		sounds.PlayMusic (Random.Range (0, 3));
	}

	void GameReset ()
	{
		model.Restart ();
		Trace.LogClear ();
	}

	void GameOver ()
	{
		model.GameOver ();
		SoundManager.StopAll ();
	}

	void ScoreUpdate ()
	{
		model.ScoreUpdate ();
	}

	void ItemPickup (GameObject item)
	{
//		sounds.PlaySound (3, 0.5f);

		model.ItemPickup (item);

//		SoundManager.PlayOneShotSound(
	}

}
