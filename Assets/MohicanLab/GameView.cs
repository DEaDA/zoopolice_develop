﻿// Author:
//       DEaDA <deada@deada.net>
// Copyright (c) 2016 Konstantin Arbuzov
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;

[System.Serializable]
public class GameView : MonoBehaviour
{
	public GameObject gameCamera;
	public Player player;
	RoadManager roadManager;
	CameraFollow cf;
	GameObject playerObject;
	public GameObject gui;

	public void Init (GameModel model, GameController controller)
	{
		name = GetType ().ToString (); 
		GameEvent.onOpenShop += onOpenShop;
		GameEvent.onGameOver += OnGameOver;
		GameEvent.onOpenCage += OpenCage;

		GameObject roadObject = new GameObject ();
		roadManager = roadObject.AddComponent<RoadManager> ();
		roadObject.transform.SetParent (this.transform, false);

		playerObject = Resources.Load ("Player/Player") as GameObject;
		playerObject = Instantiate (playerObject, new Vector3 (0, 0.2f, 1), Quaternion.identity) as GameObject;
		player = playerObject.AddComponent<Player> ();
		playerObject.transform.SetParent (this.transform, false);
		roadManager.player = playerObject.transform;

		SetupIntro ();
	}

	void onOpenShop (bool value)
	{
		if (value) {
			//			road.gameObject.SetActive (false);
			roadManager.gameObject.SetActive (false);
		} else {
			//			road.gameObject.SetActive (true);
			roadManager.gameObject.SetActive (true);
		}
	}



	void OnGameOver ()
	{
		roadManager.Reset ();
//		playerObject.transform.position = new Vector3 (0, 0.2f, 1);
		player.Reset ();
		player.Closed ();
		Destroy (cf);
		SetupIntro ();
	}

	void SetupIntro ()
	{
		gameCamera.transform.position = new Vector3 (-0.56f, 0.4f, 0.36f);
		gameCamera.transform.eulerAngles = new Vector3 (20.7f, 287.6f, 0);
	}

	void OnShakeDoor ()
	{
		roadManager.ShakeDoor ();
	}

	void OpenCage ()
	{
		roadManager.OpenCage ();
		player.RunFromCage ();
		Invoke ("ResetCamera", 0.5f);
	}

	void ResetCamera ()
	{

//		gameCamera.transform.DOMove (new Vector3 (0, 0.45f, 0.75f), 1);
		gameCamera.transform.DOMove (new Vector3 (0, 0.45f, 0.6f), 1);
		gameCamera.transform.DORotate (new Vector3 (30, 0, 0), 1);//.OnComplete (DiableFollowCamera);
		Invoke ("FollowCamera", 1f);
//		GameEvent.ResetGame ();FIXME
	}

	void FollowCamera ()
	{

		GameEvent.ResetGame ();
		cf = gameCamera.AddComponent<CameraFollow> ();
		cf.target = player.transform;
		roadManager.OnPlayerStartRun ();
	}

	void GameReset ()
	{
		roadManager.Reset ();
		player.Reset ();
		Destroy (cf);
		SetupIntro ();
	}
}

