﻿using UnityEngine;
using System.Collections;

public class GameEvent : MonoBehaviour
{
	public delegate void PlayerEvent (GameObject go);

	public delegate void ModelEvent (GameModel model);
	public delegate void UIEvent (bool value);
	public delegate void PickupEvent (GameObject coin);

	public delegate void SimpleEvent ();

	public static event ModelEvent onModelRestart;
	public static event ModelEvent onGameUpdate;

	public static event PlayerEvent onPlayerDestroyBlock;

	public static event PickupEvent onPickupObject;
	public static event SimpleEvent onScoreUpdate;
	public static event SimpleEvent onPlayerDied;
	public static event SimpleEvent onPlayerCollide;
	public static event SimpleEvent onGameReset;
	public static event SimpleEvent onStartNewGame;
	public static event SimpleEvent onBoosterComplete;
	public static event SimpleEvent onPlayerFly;
	public static event SimpleEvent onPlayerStopFly;
	public static event SimpleEvent onOpenCage;
	public static event SimpleEvent onBeginRun;
	public static event SimpleEvent onPlayerShakeDoor;
	public static event SimpleEvent onGameOver;
	public static event SimpleEvent onUseCar;
	public static event SimpleEvent onPlayerMove; 
	public static event SimpleEvent onSelectPlayerModel; 

	public static event SimpleEvent onPlayerUnlock;
	public static event UIEvent onOpenShop;

	public static void OpenShop (bool value)
	{
		if (onOpenShop != null)
			onOpenShop (value);
	}

	public static void SelectPlayerModel ()
	{
		if (onSelectPlayerModel != null)
			onSelectPlayerModel ();
	}

	public static void PlayerMove ()
	{
		if (onPlayerMove != null)
			onPlayerMove ();
	}

	public static void UseCar ()
	{
		if (onUseCar != null)
			onUseCar ();
	}

	public static void BeginRun ()
	{
		if (onBeginRun != null)
			onBeginRun ();
	}

	public static void GameOver ()
	{
		if (onGameOver != null)
			onGameOver ();
	}


	public static void PlayerUnlock ()
	{
		if (onPlayerUnlock != null)
			onPlayerUnlock ();
	}
	public static void PlayerShakeDoor ()
	{
		if (onPlayerShakeDoor != null)
			onPlayerShakeDoor ();
	}

	public static void OpenCage ()
	{
		if (onOpenCage != null)
			onOpenCage ();
	}

	public static void PlayerFly ()
	{
		if (onPlayerFly != null)
			onPlayerFly ();
	}

	public static void PlayerStopFly ()
	{
		if (onPlayerStopFly != null)
			onPlayerStopFly ();
	}

	public static void BoosterComplete ()
	{
		if (onBoosterComplete != null)
			onBoosterComplete ();
	}


	public static void ResetGame ()
	{
		if (onGameReset != null)
			onGameReset ();
	}

	public static void StartNewGame ()
	{
		if (onStartNewGame != null)
			onStartNewGame ();
	}

	public static void PlayerDied ()
	{
		if (onPlayerDied != null)
			onPlayerDied ();
	}

	public static void PlayerDestroyBlock (GameObject go)
	{
		if (onPlayerDestroyBlock != null)
			onPlayerDestroyBlock (go);
	}

	public static void PlayerCollide ()
	{
		if (onPlayerCollide != null)
			onPlayerCollide ();
	}

	public static void GameUpdate (GameModel model)
	{
		if (onGameUpdate != null)
			onGameUpdate (model);
	}

	public static void PickupObject (GameObject item)
	{
		if (onPickupObject != null)
			onPickupObject (item);
	}

	public static void ScoreUpdate ()
	{
		if (onScoreUpdate != null)
			onScoreUpdate ();
	}

	public static void ModelRestarted (GameModel model)
	{
		if (onModelRestart != null)
			onModelRestart (model);
	}
}
