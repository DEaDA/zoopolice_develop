﻿/*
Simple Sound Manager (c) 2016 Digital Ruby, LLC
http://www.digitalruby.com

Source code may no longer be redistributed in source format. Using this in apps and games is fine.
*/

using UnityEngine;
using UnityEngine.UI;

using System.Collections;

// Be sure to add this using statement to your scripts
// using DigitalRuby.SoundManagerNamespace

namespace DigitalRuby.SoundManagerNamespace
{
	public class SoundManagerDemo : MonoBehaviour
	{
		public Slider SoundSlider;
		public Slider MusicSlider;
		public InputField SoundCountTextBox;
		public Toggle PersistToggle;

		public AudioSource[] SoundAudioSources;
		public AudioSource[] MusicAudioSources;

		public void PlaySound (int index, float volume)
		{
			SoundAudioSources [index].PlayOneShotSoundManaged (SoundAudioSources [index].clip , volume);
		}

		public void PlaySound (int index)
		{
			SoundAudioSources [index].PlayOneShotSoundManaged (SoundAudioSources [index].clip );
		}


		public void PlayMusic (int index)
		{
//			Trace.Log ("PlayMusic", index);
			MusicAudioSources [index].PlayLoopingMusicManaged (0.5f, 0.5f, true);
		}

		public void SoundVolumeChanged ()
		{
			SoundManager.SoundVolume = SoundSlider.value;
		}

		public void MusicVolumeChanged ()
		{
			SoundManager.MusicVolume = MusicSlider.value;
		}
	}
}
