﻿// Author:
//       DEaDA <deada@deada.net>
// Copyright (c) 2016 Konstantin Arbuzov
using UnityEngine;
using System.Collections;
using System.Timers;

[System.Serializable]
public class GameModel  : MonoBehaviour
{

	readonly float DEFAULT_BOOSTER_TIME = 5;
	readonly float DEFAULT_SPEED = 0.05f;
	readonly float MAX_SPEED = 0.2f;

	public float lastGameSpeed = 0;
	public float gameSpeed = 0;

	public int coins = 0;
	public int score = 0;
	public int scoreMultiplier = 1;
	public int coinMultiplier = 1;
	public float playerJumpBooster = 1;
	public bool isGameOver;

	[SerializeField]
	bool _flyMode = false;
	[SerializeField]
	bool _magnetMode = false;
	[SerializeField]
	bool boosterEnabled = false;
	[SerializeField]
	float boosterTime = 0.0f;
	[SerializeField]
	float boosterTimeMax = 0;
	//10.0f;

	public void Init ()
	{
		if (PlayerPrefs.HasKey (SaveData.COINS)) {
			int coinsOld = PlayerPrefs.GetInt (SaveData.COINS);
			coins = coinsOld;
		}
	}

	void Start ()
	{
	}

	void OnValidate ()
	{
		flyMode = _flyMode;
		magnetMode = _magnetMode;
	}

	public bool flyMode {
		get {
			return _flyMode;
		}
		set {
			_flyMode = value;
			if (value) {
				gameSpeed = MAX_SPEED;
				GameEvent.PlayerFly ();
			} else {
				gameSpeed = lastGameSpeed;
				GameEvent.PlayerStopFly ();
			}
		}
	}

	public bool magnetMode {
		get {
			return _magnetMode;
		}
		set {
			_magnetMode = value;
		}
	}

	public void OnBoosterTimeComplete ()
	{
//		Trace.Log ("---------------OnBoosterTimeComplete-----------------");
		gameSpeed = lastGameSpeed;
		boosterTimeMax = DEFAULT_BOOSTER_TIME;
		playerJumpBooster = 1;
		scoreMultiplier = 1;
		coinMultiplier = 1;
		boosterEnabled = false;
		flyMode = false;
		magnetMode = false;
//		Trace.Log ("-----------------------------------------------------");
	}

	public void CarCrashed ()
	{
		gameSpeed = DEFAULT_SPEED;
		UpdateGame ();
	}

	public void GameOver ()
	{
		//MonoBehaviour.print ("Game over");
		isGameOver = true;
		gameSpeed = 0;
		UpdateGame ();
	}

	public bool CanAddBooster ()
	{
		if (boosterEnabled)
			return false;
		return true;
	}

	public void Restart ()
	{
		boosterTimeMax = DEFAULT_BOOSTER_TIME;
		magnetMode = false;
		flyMode = false;
		playerJumpBooster = 1;
		gameSpeed = lastGameSpeed = DEFAULT_SPEED;
		score = 0;
		scoreMultiplier = 1;
		coinMultiplier = 1;
		isGameOver = false;

		GameEvent.ModelRestarted (this);
		UpdateGame ();
	}

	public void BeginRun ()
	{
//		Restart ();
//		gameSpeed = lastGameSpeed = DEFAULT_SPEED;
		UpdateGame ();
	}

	public void ScoreUpdate ()
	{
		if (isGameOver)
			return;
		score += scoreMultiplier;
//		if (gameSpeed < MAX_SPEED && !boosterEnabled)
//			gameSpeed += 0.001f;
		
//		gameSpeed = 0.1f;
//		gameSpeed = 1f;//FIXME
		UpdateGame ();
	}

	public void ItemPickup (GameObject item)
	{
		if (item.name != "coin") {
			UseBooster (item.name);
		} else if (item.name == "coin") {
			AddCoin ();
		}
		UpdateGame ();
	}

	void UseBooster (string type)
	{
		boosterTime = boosterTimeMax;
		if (type == Item.SPEED) {
			lastGameSpeed = gameSpeed;
			gameSpeed = MAX_SPEED;
		} else if (type == Item.JUMP) {
			playerJumpBooster = 1.3f;
		} else if (type == Item.SCOREx2) {
			scoreMultiplier = 2;
		} else if (type == Item.COINSx2) {
			coinMultiplier = 2;
		} else if (type == Item.FLY) {
			flyMode = true;
		} else if (type == Item.MAGNET) {
			magnetMode = true;
		} else if (type == Item.MEGAJUMP) {
			flyMode = true;
			boosterTime = 1;
		}
		boosterEnabled = true;
	}

	void AddCoin ()
	{
		coins += coinMultiplier;
		PlayerPrefs.SetInt (SaveData.SCORE, coins);
	}

	public void UpdateGame ()
	{
		gameSpeed = 0;
		GameEvent.GameUpdate (this);
	}

	public void  FixedUpdate ()
	{
		if (boosterEnabled) {
			boosterTime -= Time.deltaTime;
			if (boosterTime < 0)
				OnBoosterTimeComplete ();
		}
	}
}

