﻿// Author:
//       DEaDA <deada@deada.net>
// Copyright (c) 2016 Konstantin Arbuzov
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class BaseGroup : MonoBehaviour
{
	internal List<GameObject> arr;
	internal int max = 0;
	public string path = "";
	public string side = "";
	public int current = 0;


	public  string GetPath (int pos)
	{
		string path = "Loc1/Loc01_" + side + "0";
		if (pos > 0) {
			int p = 0;
			if (pos < 5) {
				p = UnityEngine.Random.Range (0, 5);
				path = "Loc1/Loc01_" + side + p;
			}
			if (pos >= 5 && pos < 25) {
				p = UnityEngine.Random.Range (1, 5);
				path = "Loc2/Loc02_" + side + p;
			} 
			if (pos >= 25 && pos < 45) {
				p = UnityEngine.Random.Range (1, 5);
				path = "Loc3/Loc03_" + side + p;
			}
			if (pos >= 45 ) {
				p = UnityEngine.Random.Range (1, 5);
				path = "Loc4/Loc04_" + side + p;
			}
		}
		return path;
	}

	public void Setup (int max, string side = "")
	{
		name = GetType ().ToString (); 
		this.max = max;
		this.side = side;

		Fill ();
	}

	public void Fill ()
	{
		current = 0;

		GameObject obj;
		arr = new List<GameObject> (max);
		for (int i = 0; i < max; ++i) {
			obj = Utils.GetInstance ().Get (GetPath (current));
			obj.transform.localPosition = new Vector3 (0, 0, i * 1.5f);
			obj.transform.SetParent (transform, false);
			arr.Add (obj);
			current++;
		}
	}

	public void Reset ()
	{
		GameObject obj;
		for (int i = 0; i < arr.Count; ++i) {
			obj = arr [i];
			obj.SetActive (false);
		}
		arr.Clear ();
		Fill ();
	}

	public void Move (float speed)
	{
		GameObject last;
		GameObject obj;
		int i = 0;
		for (i = 0; i < arr.Count; ++i) {
			obj = arr [i] as GameObject;
			float pos = (float)Math.Round ((double)(obj.transform.localPosition.z - speed), 2);
			obj.transform.localPosition = new Vector3 (0, 0, pos);
		}
		obj = arr [0];

		if (obj.transform.localPosition.z <= -1.5f) {//FIXME random filler
			obj.SetActive (false);
			arr.RemoveAt (0);

			current++;
			if (current == 65)
				current = 5;

			obj = Utils.GetInstance ().Get (GetPath (current));
			last = arr.Last () as GameObject;
			float roadPosition = 1.5f + last.transform.localPosition.z;
			obj.transform.localPosition = new Vector3 (0, 0, roadPosition);
			obj.transform.SetParent (transform, false);
			arr.Add (obj);
		}
	}
}

