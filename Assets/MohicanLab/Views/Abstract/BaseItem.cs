﻿using UnityEngine;
using System.Collections;

public class BaseItem : MonoBehaviour
{
	public Transform skin;
	public Renderer r;
	public Vector3 pos;
	public bool pickuped;
	public string id="0";

	public virtual void Start ()
	{
		skin = GetComponentsInChildren<Transform> () [1];
		r = GetComponentInChildren<Renderer> ();

		Vector3 euler = skin.eulerAngles;
		euler.y = Random.Range (0f, 360f);
		skin.eulerAngles = euler;
	}

	void  FixedUpdate()
	{
		skin.Rotate (Vector3.up * 4);
	}
}
