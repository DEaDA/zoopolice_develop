﻿using UnityEngine;
using System.Collections;

public class Item : BaseItem
{
	string[] types = new string[]{ SPEED, JUMP, COINSx2, SCOREx2, FLY, MAGNET, MEGAJUMP };
	//FIXME check this, when added a new booster!

	public static readonly string COIN = "coin";

	public static readonly string SPEED = "speed";
	public static readonly string JUMP = "jump";
	public static readonly string SCOREx2 = "scorex2";
	public static readonly string COINSx2 = "coinsx2";
	public static readonly string FLY = "fly";
	public static readonly string MAGNET = "magnet";
	public static readonly string MEGAJUMP = "megajump";

	public RoadView road;

	public void MakeCoin ()
	{
		Reset ();
		this.name = COIN;
		Paint ();
	}

	public void MakeBooster ()
	{
		Reset ();
		int r = Random.Range (0, types.Length);
		name = types [r];
//		name = FLY;//FIXME

		Paint ();
	}

	override public void Start ()
	{
		base.Start ();
		Reset ();
		Paint ();
	}

	void Paint ()
	{

		if (r==null)
			r = GetComponentInChildren<Renderer> ();
		
		if (name == SPEED) {
			r.material.color = Color.green; 
		} else if (name == JUMP) {
			r.material.color = Color.blue; 
		} else if (name == SCOREx2) {
			r.material.color = Color.red; 
		} else if (name == COINSx2) {
			r.material.color = Color.yellow; 
		} else if (name == FLY) {
			r.material.color = Color.cyan; 
		} else if (name == MAGNET) {
			r.material.color = Color.white; 
		} else if (name == MEGAJUMP) {
			r.material.color = Color.gray; 
		} 
	}

	public void Pickup ()
	{
//		Trace.Log (road);
		road.PickupObject (gameObject);
	}

	public void Reset ()
	{
		pickuped = false;
//		road = null;
	}
}
