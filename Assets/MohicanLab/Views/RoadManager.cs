﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EpPathFinding.cs;
using System;

public class RoadManager : MonoBehaviour
{
	int max = 2;
	float score = 0;
	float scoreCounter = 0;
	float speed = 0;
	bool magnet = false;

	public Transform player;

	List<BaseGroup> fillers;
	List<RoadView> roads;
	GameModel model;

	public Animation cageAnimation;
	public  AnimationState cageAniState;
	TextMesh scoreMesh;


	public void Awake ()
	{
		GameEvent.onGameUpdate += OnGameUpdate;
		GameEvent.onPlayerDestroyBlock += OnPlayerDestroyBlock;
		GameEvent.onPlayerShakeDoor += onPlayerShakeDoor;
		GameEvent.onPlayerUnlock += onPlayerUnlock;
	}

	public void onPlayerShakeDoor ()
	{
		cageAnimation.Play ("Shake");
	}

	public void onPlayerUnlock ()
	{
		cageAnimation.Play ("Unlock");
	}

	public void OnGameUpdate (GameModel model)
	{
		this.model = model;
		speed = model.gameSpeed;
		magnet = model.magnetMode;
	}

	public void OnPlayerStartRun ()
	{
		CloseCage ();
	}

	void OnPlayerDestroyBlock (GameObject go)
	{
		for (int i = 0; i < roads.Count; i++) {
			roads [i].OnPlayerDestroyBlock (go);
		}
		model.CarCrashed ();
	}

	public void Reset ()
	{
		score = 0;
		scoreCounter = 0;

		RoadView road;
		for (int i = 0; i < roads.Count; i++) {
			road = roads [i];
			road.id = i;
			RoadView lastRoad = null;
			if (i == 0) {
				road.SetPrevModel (null);
				road.CreateGate ();
			} else {
				lastRoad = roads [i - 1] as RoadView;
				road.SetPrevModel (lastRoad.GetModel ());
			}
			road.transform.localPosition = new Vector3 (0, 0, i * RoadModel.ROWS / 2);
//			road.GetComponent<Rigidbody> ().position = road.transform.localPosition;
		}

		for (int i = 0; i < fillers.Count; i++) {
			fillers [i].Reset ();
		}

		scoreMesh.text = "My Score:\n" + PlayerPrefs.GetInt (SaveData.SCORE);

	}

	void Start ()
	{
		name = GetType ().ToString (); 
		GameObject go;
		RoadView road;
		Rigidbody rb;
		roads = new List<RoadView> ();

		for (int i = 0; i < max; i++) {
			go = new GameObject ();
			rb = go.AddComponent<Rigidbody> ();
			rb.freezeRotation = true;
			rb.useGravity = false;
			rb.angularDrag = 0;
			rb.isKinematic = true;

			road = go.AddComponent<RoadView> ();
			if (i > 0) {
				road.other = roads [i - 1].model;
			} 
			road.id = i;
			road.Init ();
			road.transform.SetParent (this.transform, false);
			road.transform.localPosition = new Vector3 (0, 0, i * RoadModel.ROWS / 2);
			roads.Add (road);

			road.player = player;

		}

		fillers = new List<BaseGroup> ();
		BaseGroup group;
		go = new GameObject ();
		group = go.AddComponent<BaseGroup> ();
		group.Setup (10);
		go.transform.SetParent (this.transform, false);
		fillers.Add (group);

		cageAnimation = group.arr [0].GetComponentInChildren<Animation> ();
		scoreMesh = group.arr [0].GetComponentInChildren<TextMesh> ();

		if (PlayerPrefs.HasKey (SaveData.SCORE)) {
			int oldScore = PlayerPrefs.GetInt (SaveData.SCORE);
			scoreMesh.text = "My Score:\n" + oldScore;
		} else {
			scoreMesh.text = "My Score:\n0";
		}

		foreach (AnimationState state in cageAnimation) {
			if (state.name == "Open") {
				cageAniState = state;
				break;
			}
		}

		go = new GameObject ();
		group = go.AddComponent<BaseGroup> ();
		group.Setup (10, "R_");
		go.transform.SetParent (this.transform, false);
		fillers.Add (group);
	}

	public void ShakeDoor ()
	{
		cageAnimation.Play ("Shake");
	}

	public void OpenCage ()
	{
		scoreMesh.text = "";
		cageAniState.speed = 1;
		cageAniState.time = 0;
		cageAnimation.Play ("Open");
	}

	public void CloseCage ()
	{
		cageAniState.speed = -10;
		cageAniState.time = cageAniState.length;
		cageAnimation.Play ("Open");
	}

	void FixedUpdate ()
	{


		if (speed == 0)
			return;

		scoreCounter += speed * 2;
		if (scoreCounter >= 0.5) {
			score += (int)scoreCounter;
			GameEvent.ScoreUpdate ();
			scoreCounter = 0;
		}

		if (fillers != null) {
			for (int i = 0; i < fillers.Count; i++) {
				fillers [i].Move (speed);
			}
		}
		RoadView road;
		if (roads != null) {
			for (int i = 0; i < max; i++) {
				road = roads [i];
//				float pos = (float)Mathf.Round (speed * 100f) / 10000f;
				float pos = (float)Math.Round ((double)(road.transform.localPosition.z - speed), 2);
//				road.transform.Translate (0, 0, road.transform.localPosition.z - pos);
				road.transform.localPosition = new Vector3 (0, 0, pos);
//				Trace.Log (pos, road.transform.localPosition.z);
				road.UseMagnet (magnet);
			}
		}

		road = roads [0];
		road.CheckDeadZone ();
		if (road.transform.localPosition.z <= -9) {
			RoadView lastRoad = roads [roads.Count - 1] as RoadView;

			roads.RemoveAt (0);
			roads.Add (road);
			road.SetPrevModel (lastRoad.GetModel ());

			float roadPosition = 9 + lastRoad.transform.position.z;
			road.transform.localPosition = new Vector3 (0, 0, roadPosition); 
			if (model.CanAddBooster () && !IsBoosterExist ())
				road.MakeBooster ();
		}
	}


	bool IsBoosterExist ()
	{
		RoadView road;
		if (roads != null) {
			for (int i = 0; i < max; i++) {
				road = roads [i];
				if (road.boosterExist)
					return true;
			}
		}
		return false;
	}
}
