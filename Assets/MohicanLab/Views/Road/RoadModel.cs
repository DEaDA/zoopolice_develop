﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EpPathFinding.cs;
using UnityEngine;

public class RoadModel
{
	public static int ROWS = 18;
	public static int COLS = 3;
	public TileModel[,] tiles;
	public int id;
	int countGeneration = 0;
	BaseGrid searchGrid;
	public List<GridPos> resultPathList;
	public GridPos startPos;
	public GridPos endPos;
	public RoadModel other;
	JumpPointParam jpParam;
	int beginDist = 10;

	bool iAllowEndNodeUnWalkable = true;
	bool iCrossCorner = false;
	bool iCrossAdjacentPoint = true;

	public void Init ()
	{
		SetupPosition ();
		SetupSearchGrid ();
		SetupTilesGrid ();
	}

	void SetupTilesGrid ()
	{
		tiles = new TileModel[COLS, ROWS];
		TileModel tile;
		for (int x = 0; x < COLS; x++) {
			for (int y = 0; y < ROWS; y++) {
				tile = new TileModel ();
				tile.x = x;
				tile.z = y;
				tiles [x, y] = tile;
			}
		}
	}

	void SetupSearchGrid ()
	{
		searchGrid = new DynamicGrid (); 
		jpParam = new JumpPointParam (searchGrid, startPos, endPos, iAllowEndNodeUnWalkable, iCrossCorner, iCrossAdjacentPoint);  //true false false TODO
	}

	public void SetupPosition ()
	{
		if (other != null)
			startPos = new GridPos (other.endPos.x, 0);
		else
			startPos = new GridPos (1, 0);
		endPos = new GridPos (Random.Range (0, COLS), ROWS - 1);
	}

	public void GeneratePath ()
	{
		countGeneration++;

		//clean last search grid
		searchGrid.Reset ();

		TileModel tile;

		int pos = 0;
		//clean field and set random blocks 
		for (int x = 0; x < COLS; x++) {

			for (int y = 0; y < ROWS; y++) {
				pos++;

				tile = tiles [x, y];
				tile.Clean ();
				searchGrid.SetWalkableAt (x, y, true);
			
				if ((x == startPos.x && y == startPos.y) || (y == endPos.y && x == endPos.x) || tile.group != null || other == null)
					continue;

				if (other != null) {
					if (Random.value > 0.8) {
						tile.block = true;
						tile.type = TileModel.TYPE.BLOCK;
						tile.Add (tile);
						searchGrid.SetWalkableAt (x, y, false);
					}
				}
			}
		}

		//creating search field params and start pathfinding
		jpParam = new JumpPointParam (searchGrid, startPos, endPos, iAllowEndNodeUnWalkable, iCrossCorner, iCrossAdjacentPoint); 
		List<GridPos> arr = JumpPointFinder.FindPath (jpParam); 

		//getting fullpath
		resultPathList = JumpPointFinder.GetFullPath (arr); 

		//repeat cycle if cannot find path
		if (resultPathList == null || resultPathList.Count == 0) {

//			Trace.Log (id, other);

			GeneratePath ();
		} else {
			//put all existing blocks to groups
			FillBlocks (); 
			CreateRamps ();
			CleanIsoaltedBlocks ();
			CreateSinleBlocks ();

			//path found 
			countGeneration = 0;
//			if (other == null) {
//				endPos = new GridPos (Random.Range (0, COLS), ROWS - 1);//TODO
//			}

//			Trace.Log (startPos.x, endPos.x);
		}
	}

	public GridPos end {
		get { 
			return endPos;
		}
	}

	public int total {
		get { 
			return ROWS * COLS;
		}
	}

	public List<TileModel> blocks {
		get { 
			List<TileModel> temp = new List<TileModel> ();
			TileModel tile;
			for (int x = 0; x < COLS; x++) {
				for (int y = 0; y < ROWS; y++) {
					tile = tiles [x, y];
					if (tile.group != null) {
						if (tile.type != TileModel.TYPE.BLOCK) {
							temp.Add (tile.group [0]);
							y += tile.group.Count - 1;//skip next tiles from this group
						} else {
							temp.Add (tile);
						}
					}
				}
			}
			return temp;
		}
	}

	bool TileExist (TileModel tile, int offsetY = 0)
	{

		GridPos pos;
		for (int i = 0; i < resultPathList.Count; i++) {
			pos = resultPathList [i];
			if (pos.y + offsetY < ROWS) {
				if (tile == tiles [pos.x, pos.y + offsetY]) {
				
					return true;
				}
			}
		}
		return false;
	}

	public TileModel GetTile (GridPos pos)
	{
		return  tiles [pos.x, pos.y];
	}

	void FillBlocks ()
	{
		TileModel tile;
		GridPos pos;

		for (int x = 0; x < COLS; x++) {
			for (int y = 0; y < ROWS; y++) {
				tile = tiles [x, y];
				if (!TileExist (tile) && tile.group == null) {

					if (other == null && y < beginDist)
						continue;
					
					if (TileExist (tile, 1) || TileExist (tile, 2))
						continue;
					
					tile.block = true;
					tile.type = TileModel.TYPE.BLOCK;
					tile.Add (tile);
				}
			}
		}
		for (int x = 0; x < COLS; x++) {
			for (int y = 0; y < ROWS; y++) {
				tile = tiles [x, y];
				if (!tile.block && y + 1 < ROWS) {
					if (x == 0 || x == 2) {
						if (!tiles [1, y + 1].block &&
						    tiles [1, y].block) {
							tiles [1, y].Clean ();
							tiles [1, y].color = Color.green;
						}
					} 
					if (x == 1) {
						if (tiles [0, y + 1].group == null &&
						    tiles [0, y].group != null) {
							tiles [0, y].Clean ();
							tiles [0, y].color = Color.green;
						}
						if (tiles [2, y + 1].group == null &&
						    tiles [2, y].group != null) {
							tiles [2, y].Clean ();
							tiles [2, y].color = Color.green;
						}
					}
				}
			}
		}
		int max = resultPathList.Count;
		for (int i = 0; i < max; i++) {
			pos = resultPathList [i];
			tile = tiles [pos.x, pos.y];
			tile.color = Color.green;
		}

		List<TileModel> group;

		for (int x = 0; x < COLS; x++) {
			for (int y = 0; y < ROWS; y++) {
				tile = tiles [x, y];
				if (y + 2 < ROWS) {
					if (tile.block && tile.type == TileModel.TYPE.BLOCK) {
						if (tiles [x, y + 1].block &&
						    tiles [x, y + 2].block &&
						    tiles [x, y + 1].type == TileModel.TYPE.BLOCK &&
						    tiles [x, y + 2].type == TileModel.TYPE.BLOCK) {
							group = new List<TileModel> { tile, tiles [x, y + 1], tiles [x, y + 2] };

							foreach (TileModel subtile in group) {//FIXME
								subtile.group = group;
								subtile.type = TileModel.TYPE.BLOCK_LONG;
							}

//							TileModel subtile;
//							for (int s = 0; s < 3; s++) {
//									subtile = group[s];
//									subtile.group = group;
//									subtile.type = TileModel.TYPE.BLOCK_LONG;
//							}
						}
					} 
				}
			}
		}
		//createManualRamps ();
	}

	void CreateSinleBlocks ()
	{
		TileModel tile;
		List<TileModel> group;
		GridPos pos;
		for (int i = 0; i < resultPathList.Count; i++) {
			pos = resultPathList [i];
			int x = pos.x;
			int y = pos.y;
			if (i > 3 && i < ROWS - 4) {
				tile = tiles [x, y];
				if (tile.type == TileModel.TYPE.EMPTY && tile.group == null) {
					TileModel prev1 = tiles [resultPathList [i - 1].x, resultPathList [i - 1].y];
					TileModel prev2 = tiles [resultPathList [i - 2].x, resultPathList [i - 2].y];
					TileModel next1 = tiles [resultPathList [i + 1].x, resultPathList [i + 1].y];
					TileModel next2 = tiles [resultPathList [i + 2].x, resultPathList [i + 2].y];
					TileModel next3 = tiles [resultPathList [i + 2].x, resultPathList [i + 3].y];
					if (prev1.x == x && prev2.x == x && next1.x == x && next2.x == x && next3.x == x) {
//						if (Random.value > 0.5) {
						tile.type = TileModel.TYPE.BLOCK;
						group = new List<TileModel> {
							prev1,
							prev2,
							next1,
							tile,
							next2,
							next3
						};
						foreach (TileModel subtile in group) {//FIXME
							subtile.group = group;
						}
//						}
					}
				}
			}
		}
	}

	void CleanIsoaltedBlocks ()
	{
		TileModel tile;
		for (int x = 0; x < COLS; x++) {
			for (int y = 0; y < ROWS; y++) {
				tile = tiles [x, y];
				if (tile.type == TileModel.TYPE.BLOCK) {
					tile.Clean ();
				}
			}
		}
	}

	public 	List<TileModel> getRampPath ()
	{
		List<TileModel> path = new List<TileModel> ();

		TileModel tile;
		bool unblock = true;
		for (int x = 0; x < COLS; x++) {
			for (int y = 0; y < ROWS; y++) {
				tile = tiles [x, y];
				if (tile.type == TileModel.TYPE.BLOCK_RAMP) {
					y += 2;
					unblock = false;
				} else if (tile.type == TileModel.TYPE.BLOCK_LONG && !unblock) {
					path.Add (tile.group [0]);
					path.Add (tile.group [1]);
					path.Add (tile.group [2]);
					y += 2;
				} else {
					unblock = true;
				}
			}
		}

		return path;
	}

	void CreateManualRamps ()
	{
		int x = 0;
		TileModel tile;
		List<TileModel> group;

		for (int y = 0; y < ROWS; y++) {
			tile = tiles [x, y];
			///fill line by ramp +trains
			tile = tiles [x, y];
			if (y + 2 < ROWS) {
				if (tile.block && tile.type == TileModel.TYPE.BLOCK) {
					if (tiles [x, y + 1].block &&
					    tiles [x, y + 2].block &&
					    tiles [x, y + 1].type == TileModel.TYPE.BLOCK &&
					    tiles [x, y + 2].type == TileModel.TYPE.BLOCK) {
						group = new List<TileModel> { tile, tiles [x, y + 1], tiles [x, y + 2] };
						foreach (TileModel subtile in group) {//FIXME
							subtile.group = group;
							subtile.type = TileModel.TYPE.BLOCK_RAMP;
						}
					}
				} 
			}
		}
	}

	void CreateRamps ()
	{
		List<TileModel> group;
		TileModel tile;
//		TileModel nextGroupTile;
		for (int x = 0; x < COLS; x++) {
			for (int y = 0; y < ROWS; y++) {
				
				if (tiles [x, y].type == TileModel.TYPE.BLOCK_LONG) {
					tile = tiles [x, y].group [0];
					if (y + 3 < ROWS && y - 1 > 0) {
						if (tiles [x, y - 1].group == null) {
							if (tiles [x, y].type != TileModel.TYPE.BLOCK_RAMP && tiles [x, y + 3].type == TileModel.TYPE.BLOCK_LONG) {
								if (Random.value > 0.5) {
									group = new List<TileModel> { tile, tiles [x, y + 1], tiles [x, y + 2] };
									foreach (TileModel subtile in group) {//FIXME
										subtile.group = group;
										subtile.type = TileModel.TYPE.BLOCK_RAMP;
									}
								}
							}
						}
					}
				}
			}
		}
	}

}

/* DEAD CODE

void createRamps ()
{
	//		TileModel tile;
	List<int> t = new List<int> ();


	for (int y = 0; y < ROWS; y++) {
		//				tile = tiles [x, y];
		if (tiles [0, y].block && tiles [1, y].block && tiles [2, y].block)
			t.Add (y);
		if (y + 1 < ROWS) {
			if (tiles [0, y].block && tiles [1, y].block && tiles [2, y + 1].block) {
				t.Add (y);
			}
			if (tiles [0, y].block && tiles [1, y + 1].block && tiles [2, y + 1].block) {
				t.Add (y);
			}
			if (tiles [0, y].block && tiles [1, y + 1].block && tiles [2, y].block) {
				t.Add (y);
			}
			if (tiles [0, y + 1].block && tiles [1, y].block && tiles [2, y].block) {
				t.Add (y);
			}
		}

	}
	if (t.Count > 0) {
		t.Sort ();
		int py = t [0];
		MonoBehaviour.print (py);

		TileModel prevTile;
		TileModel firstInGroup;
		TileModel tile;
		List<TileModel> group;

		bool created = false;
		for (int px = 0; px < 3; px++) {
			tile = tiles [px, py];
			if (tile.block == true) {
				firstInGroup = tile.group [0];
				if (firstInGroup.z - 1 > 0) {
					prevTile = tiles [px, firstInGroup.z - 1];
					if (prevTile.block == false) {
						group = tile.group;
						foreach (TileModel gt in group) {//FIXME
							gt.type = TileModel.TYPE.BLOCK_RAMP;

						}
						created = true;
						break;
					}
				}
			}
		}
		if (!created) {
			for (int px = 0; px < 3; px++) {
				tile = tiles [px, py];
				if (tile.block != true) {
					if (py + 1 < ROWS) {
						prevTile = tiles [px, py + 1];
						if (prevTile.block == true) {
							group = prevTile.group;
							foreach (TileModel gt in group) {//FIXME
								gt.type = TileModel.TYPE.BLOCK_RAMP;

							}
							break;
						}
					}

				}
			}		
		}
		_debugPY = py;
	}

}

public int _debugPX = 0;
public int _debugPY = 0;

*/
//		createRamps ();
//
//
//		for (int x = 0; x < COLS; x++) {
//			for (int y = 0; y < ROWS; y++) {
//				tile = tiles [x, y];
//				if (tile.type != TileModel.TYPE.BLOCK_RAMP) {
//					searchGrid.SetWalkableAt (x, y, false);
//				} else {
//					finish = new GridPos (tile.x, tile.group[0].z);
//				}
//			}
//		}
//TODO

/*

				if (Random.value > 0.91) {

					TileModel next;
					TileModel next1;
					TileModel next2;
					TileModel next3;


					if (y + 2 < ROWS) {
						next = tiles [x, y + 1];
						next2 = tiles [x, y + 2];

						List<TileModel> group = new List<TileModel> { tile, next, next2 };
						foreach (TileModel t in group) {//FIXME
							t.block = true;
							t.group = group;
							t.type = TileModel.TYPE.BLOCK_LONG;
//							if (Random.value > 0.5) {
//								t.type = TileModel.TYPE.BLOCK_RAMP;
//								if (y + 2 + 3 < ROWS) {
//
//								}
//							}


						}
						if (y + 5 < ROWS) {
							next1 = tiles [x, y + 3];
							next2 = tiles [x, y + 4];
							next3 = tiles [x, y + 5];
							group = new List<TileModel> { next1, next2, next3 };
							foreach (TileModel t in group) {//FIXME
								t.block = true;
								t.group = group;
								t.type = TileModel.TYPE.BLOCK_LONG;
							}
						}




//						searchGrid.SetWalkableAt (x, y, false);
//						searchGrid.SetWalkableAt (x, y + 1, false);
//						searchGrid.SetWalkableAt (x, y + 2, false);

					}
				}


				*/

//				else {
//					tile.block = true;
////					searchGrid.SetWalkableAt (x, y, false);
//					List<TileModel> group = new List<TileModel> { tile };
//					tile.group = group;
//				}

//					tile.block = true;
//					searchGrid.SetWalkableAt (x, y, false);
//				}
//				if (!tile.block) {
//					if (Random.value > 0.9) {
//						if (tiles [x, y + 1] != null && tiles [x, y + 2] != null) {
//							tile.block = true;
//							tiles [x, y + 1].block = true;
//							tiles [x, y + 2].block = true;
//							searchGrid.SetWalkableAt (x, y, false);
//							searchGrid.SetWalkableAt (x, y+1, false);
//							searchGrid.SetWalkableAt (x, y+2, false);
//						}
//					}
//				}
//}
//}
