﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EpPathFinding.cs;

using DG.Tweening;

public class RoadView : MonoBehaviour
{
	public int id;
	public RoadModel other;
	public RoadModel model;
	public RoadManager parent;
	public bool boosterExist = false;
	public Transform player;

	List<TileView> tiles;
	List<GameObject> blocks;
	List<GameObject> items;

	GameObject zooGate;

	public RoadModel GetModel ()
	{
		return model;
	}

	public void SetPrevModel (RoadModel other)
	{
		this.other = other;
		model.id = id;
		model.other = other;
		model.SetupPosition ();
		Create ();
	}


	public void Init ()
	{
		model = new RoadModel ();
		model.id = id;
		model.other = other;
		model.Init ();
	}

	public void Start ()
	{	
		tiles = new List<TileView> (); 
		name = GetType ().ToString (); 
		GameObject obj;

		for (int x = 0; x < RoadModel.COLS; x++) {
			for (int y = 0; y < RoadModel.ROWS; y++) {

				TileView tileview = new TileView ();
				tileview.x = x;
				tileview.y = y;
				tiles.Add (tileview);

				if (x == 0) {
					obj = Utils.GetInstance ().Get (Utils.ROAD);
					obj.transform.SetParent (transform, false);
					obj.transform.localPosition = new Vector3 (0, 0, -0.5f + y * 0.5f);

				}
			}
		}
		Create ();
	}

	public void Clean ()
	{
		if (zooGate != null) {
			zooGate.SetActive (false);
		}
		if (items != null) {
			GameObject coin;
			for (int i = 0; i < items.Count; i++) {
				coin = items [i];
				coin.SetActive (false);
				coin.GetComponent<Item> ().Reset ();
			}
			items.Clear ();
		}
		if (blocks != null) {
			GameObject block;
			for (int i = 0; i < blocks.Count; i++) {
				block = blocks [i];
				block.SetActive (false);
			}
			blocks.Clear ();
		}
	}

	public void PickupObject (GameObject pickupObject)
	{
		if (pickupObject.GetComponent<Item> ().pickuped) {
			return;
		}
		if (items.IndexOf (pickupObject) != -1) {
			if (pickupObject.name != Item.COIN) {
				boosterExist = false;
			}
			DOTween.Kill (pickupObject);
			GameEvent.PickupObject (pickupObject);
			items.RemoveAt (items.IndexOf (pickupObject));
			pickupObject.SetActive (false);
		} else {
			#if UNITY_EDITOR
			Time.timeScale = 0;
//			UnityEditor.EditorApplication.isPlaying = false;
//			Trace.Log (pickupObject);
			#endif
		}
	}

	public void OnPlayerDestroyBlock (GameObject go)
	{
		GameObject block;
		for (int i = 0; i < blocks.Count; i++) {
			block = blocks [i];
			if (block == go) {
				block.SetActive (false);
				blocks.RemoveAt (i);
			}
		}
	}

	public void UseMagnet (bool value)
	{
		if (!value)
			return;
		if (items != null) {
			GameObject coin;
			for (int i = 0; i < items.Count; i++) {
				coin = items [i];
				if (coin.name == Item.COIN) {
					float dist = Vector3.Distance (player.position, coin.transform.position);
					if (dist <= 1.5f) {
						if (!DOTween.IsTweening (coin.transform)) {
							coin.transform.DOMove (player.position, 0.3f);
						}
					}
				}
			}
		}
	}

	public void Create ()
	{
		if (tiles == null)
			return;

		Clean ();

		boosterExist = false;
		model.GeneratePath ();

		blocks = new List<GameObject> ();
		items = new List<GameObject> ();

//		return;
		GameObject obj;
		TileModel tile;
		GridPos pos;
		for (int i = 0; i < model.resultPathList.Count; i++) {
			pos = model.resultPathList [i];
			foreach (TileView tileView in tiles) {//FIXME
				if (pos.x == tileView.x && pos.y == tileView.y) {
					TileModel _block = model.GetTile (pos);
					if (_block != null && _block.group != null && _block.type == TileModel.TYPE.BLOCK) {
						continue;
					}
					obj = Utils.GetInstance ().Get (Utils.COIN);
					obj.transform.SetParent (transform, false);
					obj.transform.localPosition = new Vector3 (-0.5f + pos.x * 0.5f, 0, -0.5f + pos.y * 0.5f);

					Item coin = obj.GetComponent<Item> ();
					if (coin == null)
						coin = obj.AddComponent<Item> ();
					coin.MakeCoin ();
					coin.road = this;
					items.Add (obj);
					coin.id = "id:" + id + "_" + i;
				}
			}
		}
	
		TileModel ramptile;
		List <TileModel> ramps = model.getRampPath ();
		for (int i = 0; i < ramps.Count; i++) {
			ramptile = ramps [i];
			foreach (TileView tileView in tiles) {//FIXME
				if (ramptile.x == tileView.x && ramptile.z == tileView.y) {

					obj = Utils.GetInstance ().Get (Utils.COIN);
					obj.transform.SetParent (this.transform, false);
					obj.transform.localPosition = new Vector3 (-0.5f + ramptile.x * 0.5f, 0.55f, -0.5f + ramptile.z * 0.5f);

					Item coin = obj.GetComponent<Item> ();
					if (coin == null)
						coin = obj.AddComponent<Item> ();
					coin.MakeCoin ();
					coin.pos = obj.transform.position;
					coin.road = this;
					items.Add (obj);
				}
			}
		}
		string prevName = "";

		for (int i = 0; i < model.blocks.Count; i++) {

			tile = model.blocks [i];
			string name = "";
			float posOffset = 0.0f;
			string blockName = "block";

			if (tile.type == TileModel.TYPE.BLOCK) {
				posOffset = 0.0f;
				string[] shorts = new string[] {
					Utils.BLOCK_SHORT,
//					Utils.BLOCK_HIGH,
					Utils.BLOCK_UNDER,
					Utils.BLOCK_UNDER2
				};
				name = shorts [Random.Range (0, shorts.Length)];
			} else {
				switch (tile.type) {
				case TileModel.TYPE.BLOCK_LONG:
					if (prevName != "ramp") {
						posOffset = 0.5f;
						string[] longs = new string[] {
							Utils.BLOCK_LONG1,
							Utils.BLOCK_LONG2,
							Utils.BLOCK_LONG3,
							Utils.BLOCK_LONG4,
							Utils.BLOCK_LONG5,
							Utils.BLOCK_LONG6
						};
						name = longs [Random.Range (0, longs.Length)];
					}
					break;
				case TileModel.TYPE.BLOCK_RAMP:
					blockName = "ramp";
					posOffset = 0.5f;
					string[] _ramps = new string[] {
						Utils.BLOCK_RAMP1,
						Utils.BLOCK_RAMP2
					};
					name = _ramps [Random.Range (0, _ramps.Length)];
					break;
				}
			}
			if (name != "") {
				obj = Utils.GetInstance ().Get (name);
				obj.transform.SetParent (this.transform, false);
				obj.transform.localPosition = new Vector3 (-0.5f + tile.x * 0.5f, 0, -0.5f + tile.z * 0.5f + posOffset);
				obj.name = blockName;
				blocks.Add (obj);
			}
			prevName = name;
		}
		CreateGate ();
	}

	public void CreateGate ()
	{
		if (id == 0 && zooGate == null) {
			zooGate = Utils.GetInstance ().Get (Utils.ZOO_GATE);
			zooGate.transform.SetParent (this.transform, false);
			zooGate.transform.localPosition = new Vector3 (0, 0, 4.35f);
		}
	}

	public void MakeBooster ()
	{
		if (items != null && items.Count > 0) {
			int pos = Random.Range (0, items.Count);

			GameObject coinObj = items [pos];
			GameObject obj = Utils.GetInstance ().Get (Utils.BOOSTER);
			obj.transform.SetParent (transform, false);
			obj.transform.localPosition = new Vector3 (coinObj.transform.localPosition.x, coinObj.transform.localPosition.y, coinObj.transform.localPosition.z);

			Item booster = obj.GetComponent<Item> ();
			if (booster == null)
				booster = obj.AddComponent<Item> ();
			booster.MakeBooster ();
			booster.pos = obj.transform.position;
			booster.road = this;

			coinObj.SetActive (false);

			items.RemoveAt (pos);
			items.Add (obj);

			boosterExist = true;
		}
	}

	public void  CheckDeadZone ()
	{
		CheckZone (items);
		CheckZone (blocks);
	}

	public void  CheckZone (List<GameObject> objects)
	{
		if (objects != null) {
			GameObject go;
			for (int i = 0; i < objects.Count; i++) {
				go = objects [i];
				if (go.transform.position.z < -2) {
					go.SetActive (false);
					objects.RemoveAt (i);
				}
			}
		}
	}

	//	void  ()
	//	{
	//		DrawDebugPath ();
	//	}

	void DrawDebugPath ()
	{

		Color color = Color.red;
		if (id == 1)
			color = Color.yellow;
		else if (id == 2)
			color = Color.green;

		if (model.resultPathList != null) {

			int count = model.resultPathList.Count;
			float pad = 0.5f;
			GridPos to;
			GridPos from;
			Vector3 positionFrom = new Vector3 (-pad, pad, -pad);
			Vector3 positionTo = new Vector3 (-pad, pad, -pad);

			from = model.resultPathList [0];
			to = model.resultPathList [0];
			positionFrom.x = transform.position.x - pad + from.x * pad;
			positionFrom.y = transform.position.y + pad;
			positionFrom.z = transform.position.z - pad + from.y * pad - pad / 2;
			positionTo.x = transform.position.x - pad + to.x * pad;
			positionTo.y = transform.position.y + pad;
			positionTo.z = transform.position.z - pad + to.y * pad;
			Debug.DrawLine (positionFrom, positionTo, color);

			for (int i = 0; i < count; i++) {
				to = model.resultPathList [i];
				if (i > 0) {
					from = model.resultPathList [i - 1];
					to = model.resultPathList [i];
					positionFrom.x = transform.position.x - pad + from.x * pad;
					positionFrom.y = transform.position.y + pad;
					positionFrom.z = transform.position.z - pad + from.y * pad;
					positionTo.x = transform.position.x - pad + to.x * pad;
					positionTo.y = transform.position.y + pad;
					positionTo.z = transform.position.z - pad + to.y * pad;
					Debug.DrawLine (positionFrom, positionTo, color);
				} 
			}
			positionFrom.x = positionTo.x;
			positionFrom.y = positionTo.y;
			positionFrom.z = positionTo.z;
			positionTo.z = positionTo.z + pad / 2;
			Debug.DrawLine (positionFrom, positionTo, color);
		}
	}
}
