﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;

[System.Serializable]
public class Player : MonoBehaviour
{
	readonly float[] positionsPoints = new float []{ -0.5f, 0, 0.5f };

	Rigidbody playerBody;
	GameObject carObject;
	Vector3 startPosition;
	Vector3 fp;
	Vector3 lp;

	enum CurrentPosition
	{
		LEFT,
		MID,
		RIGHT
	}

	enum Direction
	{
		LEFT,
		RIGHT

	}

	string DRIVE = "";

	readonly string COLLISION = "Crash";
	readonly string IDLE = "Idle";
	readonly string JUMP = "Jump";
	readonly string LEFT = "Left";
	readonly string RIGHT = "Right";
	readonly string LOOK = "Look";
	readonly string RUN = "Run";
	//	readonly string RUNAWAY = "Runaway";
	readonly string SHAKE = "Shake";
	readonly string SLIDE = "Slide";
	readonly string UNLOCK = "Unlock";

	//	[SerializeField]
	readonly float MAX_FLY = 2.5f;
	readonly float ONFLY_DRAG = 2;
	readonly float DEFAULT_DRAG = 1.1f;
	readonly int DEFAULT_LIFE = 2;

	bool gameOver;
	public bool hasCar = false;
	public bool grounded = true;
	public bool movementComplete = true;
	public bool fly = false;
	public bool wasFlight = false;

	public int life = 0;
	public float flyPos = 0;
	public float playerJumpBooster = 1;
	public float jumpPower = 4.5f;//3.5f;
	public float movementSpeed = 0.2f;
	public int prevPosition = (int)CurrentPosition.MID;
	public int position = (int)CurrentPosition.MID;
	public int tapCount = 0;

	public bool intro = true;
	public CapsuleCollider coll;

	GameObject shadow;
	public GameObject spine;
	[SerializeField]
	public bool _sp = true;

	//	Vector3 defaultColliderPos;
	float defaultColliderHeight;
	Transform skin;
	GameObject playerModel;
	Animation playerAnimation;

	void Awake ()
	{
		GameEvent.onUseCar += UseCar;
		GameEvent.onGameUpdate += GameUpdate;
		GameEvent.onBoosterComplete += onBoosterComplete;
		GameEvent.onSelectPlayerModel += LoadPlayer;
		coll = GetComponent<CapsuleCollider> ();
		skin = GetComponentsInChildren<Transform> () [1];
		defaultColliderHeight = coll.height;

		LoadScooter ();
		LoadPlayer ();
	}

	public void LoadScooter ()
	{
		if (scooterModel != null)
			scooterModel.SetActive (false);

		scooterModel = Utils.GetInstance ().LoadCurrentScooter ();
		scooterModel.transform.position = new Vector3 (0.01f, 0, -0.03f);
		scooterModel.transform.SetParent (skin.transform, false);

		scooterAnimation = scooterModel.GetComponentsInChildren<Animation> () [0];
		foreach (AnimationState state in scooterAnimation) {
			state.speed = 1f;
			if (state.name == "Slide")
				state.speed = 1.5f;
			if (state.name == "Jump") {
				state.speed = 1;
				jumpState = state;
			}
			if (state.name == "Run")
				state.speed = 1.2f;

			if (state.name == LEFT || state.name ==RIGHT )
				state.speed = 1.5f;
			
		}
		scooterModel.SetActive (false);
	}

	AnimationState jumpState;

	public void LoadPlayer ()
	{
		if (playerModel != null)
			playerModel.SetActive (false);
	
		playerModel = Utils.GetInstance ().LoadCurrentModel ();
		playerModel.transform.SetParent (skin.transform, false);

		playerAnimation = playerModel.GetComponentsInChildren<Animation> () [0];
		foreach (AnimationState state in playerAnimation) {
			state.speed = 1f;
			//			if (state.name == LEFT || state.name == RIGHT)
			//				state.speed = 1.5f;
			if (state.name == "Fly")
				state.speed = 0.3f;
			if (state.name == "Slide")
				state.speed = 1.5f;
			if (state.name == "Jump")
				state.speed = 1.2f;
			if (state.name == "Run")
				state.speed = 1.2f;
		}
		Play ("Idle");
	}

	void UseCar ()
	{
		scooterModel.SetActive (true);
	}

	void Start ()
	{
		name = GetType ().ToString (); 
		playerBody = GetComponentsInChildren<Rigidbody> () [0];

		GameObject __sh = Resources.Load ("Player/PlayerShadow") as GameObject;
		shadow = Instantiate (__sh, new Vector3 (-0.06f, 0, 0), Quaternion.identity) as GameObject;
		shadow.transform.SetParent (playerBody.transform, false);
		shadow.name = "shadow";
		shadow.SetActive (true);//FIXME

		Reset ();

		Play (IDLE);
	}

	void onBoosterComplete ()
	{
		playerBody.drag = ONFLY_DRAG;
	}

	public void GameUpdate (GameModel model)
	{
		if (intro)
			return;

		gameOver = model.isGameOver;
		playerJumpBooster = model.playerJumpBooster;
		fly = model.flyMode;

		if (fly) {
			flyPos = playerBody.transform.position.y;
			wasFlight = true;
		} else {
			flyPos = 0;
			if (wasFlight) {
				if (playerBody != null)
					playerBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
				playerBody.rotation = Quaternion.identity;
			}
		}
	}

	public void Closed ()
	{
		Play (IDLE);
	}

	public void Reset ()
	{
		playerBody.constraints = RigidbodyConstraints.FreezeRotationZ;
//		playerBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;

		intro = true;

		prevPosition =	position = (int)CurrentPosition.MID;
		movementComplete = true;
		grounded = true;
		life = DEFAULT_LIFE;
		playerJumpBooster = 1;
		flyPos = 0;
		tapCount = 0;
		wasFlight = false;

		playerBody.drag = DEFAULT_DRAG;
		playerBody.rotation = Quaternion.identity;
		playerBody.transform.eulerAngles = new Vector3 (0, 90, 0); 
		playerBody.position = new Vector3 (-1.11f, 0.04f, 0.5f);//FIXME
		shadow.SetActive (true);
		//shadow.transform.position = new Vector3 (0.0f, 0, 0);
//		playerBody.position = new Vector3 (0, 0.0f, 1f);//FIXME

		playerAnimation.gameObject.SetActive (true);

		DRIVE = "";
		scooterModel.SetActive (false);


		Play (IDLE);

	
	}

	public void Ready ()
	{
		intro = false;

		playerBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;

		prevPosition =	position = (int)CurrentPosition.MID;
		movementComplete = true;
		grounded = true;
		life = DEFAULT_LIFE;
		playerJumpBooster = 1;
		flyPos = 0;
		tapCount = 0;
		wasFlight = false;

		playerBody.drag = DEFAULT_DRAG;
		playerBody.rotation = Quaternion.identity;
		playerBody.position = new Vector3 (0, 0.0f, 1f);//FIXME
		shadow.SetActive (true);

		scooterModel.SetActive (false);
//		DRIVE = "Drive";

		Play (RUN);
	}

	Animation scooterAnimation;
	GameObject scooterModel;

	void OnCollisionExit (Collision collisionInfo)
	{
		if (intro)
			return;

		shadow.SetActive (false);
		grounded = false;
		playerBody.transform.rotation = Quaternion.identity;
		if (!fly)
			playerBody.velocity = new Vector3 (playerBody.velocity.x, playerBody.velocity.y - 0.02f, playerBody.velocity.z);
	}

	void OnCollisionStay (Collision collisionInfo)
	{
		if (intro)
			return;
		
		grounded = true;
		if (life > 0)
			shadow.SetActive (true);
		
		if (wasFlight) {
//			GameEvent.PlayerStopFly ();
			wasFlight = false;
			playerBody.drag = DEFAULT_DRAG;
		}
		if (collisionInfo.transform.name == "Skin")
			playerBody.transform.eulerAngles = new Vector3 (-20, 0, 0); 
	}

	void OnCollisionEnter (Collision collisionInfo)
	{
		if (intro)
			return;
		
//		playerAnimation. Blend ("Run");
		bool collide = false;
		if (collisionInfo.transform.name == "SideBlock" || collisionInfo.transform.name == "Block") {
			float angle = Vector3.Angle (collisionInfo.contacts [0].normal, Vector3.forward);
			life--;
			collide = true;
			transform.DOKill ();
			if (Mathf.Approximately (angle, 90)) {
				Vector3 cross = Vector3.Cross (Vector3.forward, collisionInfo.contacts [0].normal);
				movementComplete = false;

				if (cross.y > 0) {
					if (position == (int)CurrentPosition.LEFT) {
						position = (int)CurrentPosition.MID;
					} else if (position == (int)CurrentPosition.MID) {
						position = (int)CurrentPosition.RIGHT;
					}
				} else {
					if (position == (int)CurrentPosition.RIGHT) {
						position = (int)CurrentPosition.MID;
					} else if (position == (int)CurrentPosition.MID) {
						position = (int)CurrentPosition.LEFT;
					}
				}

				playerBody.transform.DOMoveX (positionsPoints [position], movementSpeed * 2).OnComplete (OnCompleteMovement);
				playerBody.transform.DOMoveZ (1, movementSpeed);
				GameEvent.PlayerCollide ();
			} else {
				life = 0;
			}
		}  
		if (collide && life <= 0) {
			if (hasCar) {

				//car.gameObject.SetActive (false);
				playerAnimation.gameObject.SetActive (true);

				hasCar = false;
				GameEvent.PlayerDestroyBlock (collisionInfo.gameObject.transform.parent.gameObject);
			} else {

				shadow.SetActive (false);
				GameEvent.PlayerDied ();
				Play (COLLISION);
			}
		}
	}

	void OnTap ()
	{
//		tapCount++;
//		if (tapCount >= 2) {
//			hasCar = true;
//			car.SetActive (true);
//			tapCount = 0;
	}

	void ChangePosition (int direction)
	{
		if (intro)
			return;
		
		if (!movementComplete || gameOver)
			return;
		if (direction == (int)Direction.LEFT) {
			if (position == (int)CurrentPosition.RIGHT) {
				position = (int)CurrentPosition.MID;
				DoMove ();
				Play (LEFT);
			} else if (position == (int)CurrentPosition.MID) {
				position = (int)CurrentPosition.LEFT;
				DoMove ();
				Play (LEFT);
			}
		} else {
			if (position == (int)CurrentPosition.LEFT) {
				position = (int)CurrentPosition.MID;
				DoMove ();
				Play (RIGHT);
			} else if (position == (int)CurrentPosition.MID) {
				position = (int)CurrentPosition.RIGHT;
				DoMove ();
				Play (RIGHT);
			}
		}
	}

	void DoMove ()
	{
		movementComplete = false;
		playerBody.transform.DOMoveX (positionsPoints [position], movementSpeed).OnComplete (OnCompleteMovement);
		GameEvent.PlayerMove ();
	}

	void MoveLeft ()
	{
		ChangePosition ((int)Direction.LEFT);
	}

	void MoveRight ()
	{
		ChangePosition ((int)Direction.RIGHT);
	}

	void Jump ()
	{
		if (intro || !grounded)
			return;
		
		Play (JUMP);
		playerBody.velocity = new Vector3 (playerBody.velocity.x, jumpPower * playerJumpBooster, playerBody.velocity.z);
	}

	void RestoreHight ()
	{
		coll.height = defaultColliderHeight;
		playerBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
	}

	void Duck ()
	{
		if (intro || !grounded)
			return;
		Play (SLIDE);

		playerBody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;

		coll.height = 0.2f;
		Invoke ("RestoreHight", 0.4f);
	}

	#if UNITY_ANDROID || UNITY_IPHONE
	Touch[] touches;
	int maxTouches;
	#endif

	float idleTime = 1;

	public void RunFromCage ()
	{
		intro = false;
		Play (IDLE);
		Invoke ("RunAway", 0.5f);
	}

	void RunAway ()
	{
		Play (RUN);
		playerBody.transform.DORotateQuaternion (Quaternion.identity, 2f);//.SetDelay (1);
//		playerBody.transform.DOMove (new Vector3 (0, 0, playerBody.transform.position.z), 1f);
		playerBody.transform.DOMove (new Vector3 (0, 0, 1f), 1f).OnComplete (OnCompleteRunaway);
	}

	void OnCompleteRunaway ()
	{
		Ready ();
//		scooterModel.SetActive (true);
		scooterModel.SetActive (false);
		GameEvent.BeginRun ();
//		GameEvent.GameUpdate ();
	}

	void Update ()
	{
//		spine.SetActive (_sp);

		if (intro) {
			idleTime -= Time.deltaTime;
		
			if (idleTime < 0) {
				if (Random.value > 0.5f) {
					Play (SHAKE);
					GameEvent.PlayerShakeDoor ();
				} else {
					if (Random.value > 0.5f) {
						Play (UNLOCK);
						GameEvent.PlayerUnlock ();
					} else {
						Play (LOOK);
					}

				}
				idleTime = 5;
			}
			if (!playerAnimation.isPlaying)
				Play (IDLE);
		}

		if (gameOver)
			return;

		if (intro)
			return;
		

		#if UNITY_ANDROID || UNITY_IPHONE

		touches = Input.touches;
		maxTouches = touches.Length;
		Touch touch;

		for (int i = 0; i < maxTouches; i++) { 
			touch = touches [i];
			if (touch.phase == TouchPhase.Began) { //check for the first touch
				fp = touch.position;
				lp = touch.position;
			}
			if (touch.phase == TouchPhase.Ended) { //check if the finger is removed from the screen
				lp = touch.position;
				if (Mathf.Abs (lp.x - fp.x) > Mathf.Abs (lp.y - fp.y)) {  
					if ((lp.x > fp.x)) {  
						MoveRight ();
					} else {  
						MoveLeft ();
					}
				} else {   
					if (lp.y > fp.y) {  
						Jump ();
					} else {   
						Duck ();
					}
				}
			} else {   //It's a tap as the drag distance is less than 20% of the screen height
				OnTap ();//FIXME 2 TAPS
			}
		}
		#endif
		#if UNITY_EDITOR
		if (Input.GetKeyDown (KeyCode.Space))
			Jump ();
		if (Input.GetKeyDown (KeyCode.S))
			Duck ();
		if (Input.GetKeyDown (KeyCode.A))
			MoveLeft ();
		if (Input.GetKeyDown (KeyCode.D))
			MoveRight ();
		if (Input.GetKeyDown (KeyCode.E))
			OnTap ();
		#endif

		if (fly) {
			playerAnimation.CrossFade ("Fly");
			if (flyPos < MAX_FLY) {
				flyPos += 0.1f;
			} else {
				flyPos = MAX_FLY;
				playerBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
			}
			playerBody.transform.eulerAngles = new Vector3 (90, 0, 0); 
			playerBody.transform.position = new Vector3 (playerBody.transform.position.x, flyPos, playerBody.transform.position.z);//TODO ?
		}
//		else {
//			if (!grounded) {
//			}
//		}

		if (!playerAnimation.isPlaying) {
			Play (RUN);
		}

		if (shadow != null)
			shadow.SetActive (false);
		
//		shadow.transform.eulerAngles = new Vector3 (playerBody.rotation.x, 0, 0); 
//		shadow.transform.position = new Vector3 (playerBody.transform.position.x, 0, playerBody.transform.position.z);//TODO ?
	}

	void Play (string value)
	{
		if (DRIVE.Length > 0) {
			scooterModel.SetActive (true);

			int pos = Random.Range (1, 4);
			if (value == RUN) {
				playerAnimation.CrossFade (DRIVE + value + pos);
				scooterAnimation.CrossFade (DRIVE + pos);
			} else {
				playerAnimation.CrossFade (DRIVE + value);
				scooterAnimation.CrossFade (DRIVE + value);
			}
//			Trace.Log (">>>>>>>>>:", DRIVE + value + pos);
		} else {
			scooterAnimation.Stop ();

		
//			if (value == JUMP) {
//				playerAnimation.Play (value);
//			} else {
				playerAnimation.CrossFade (value);
//			}

			scooterModel.SetActive (false);
//			Trace.Log ("----------play animation only player:", value);
		}
	}

	void OnCompleteMovement ()
	{
		movementComplete = true;
	}

	void OnTriggerEnter (Collider item)
	{
		if (item != null)
			item.GetComponent<Item> ().Pickup ();
	}

}
