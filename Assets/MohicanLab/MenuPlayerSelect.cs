﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class MenuPlayerSelect : MonoBehaviour
{
	public  GameObject renderTexture;
	Animation ani;
	Transform skin;

	[SerializeField]
	public GameObject AnimalsMenu;
	[SerializeField]
	public GameObject ScooterMenu;
	[SerializeField]
	public GameObject AwardsMenu;
	[SerializeField]
	public GameObject currentMenu;
	[SerializeField]
	public GameObject manager;

	float idleTime = 1;
	private List<string> models = new List<string> (){ Utils.KENG, Utils.CROC, Utils.PAND };
	public string tempModelName;
	public int tempSkinIndex = 0;
	public int currentScooterIndex = 0;
	public GameObject scroller;
	public Camera cam;
	//	bool scooter = !true;

	public GameObject previewCont;
	int tempModelIndex = 0;
	RenderTexture rt;
	RawImage raw;
	GameObject _preview;
	GameObject model;


	public Text skinName;

	void Awake ()
	{
		tempSkinIndex = Utils.currentSkinIndex;
		tempModelName = Utils.currentModelName;

		Trace.Log (tempSkinIndex, tempModelName);

		ScrollRect rect = scroller.GetComponent<ScrollRect> ();
		rect.content.SetSizeWithCurrentAnchors (0, 160 * models.Count - 10);
		Button btn;
		for (int i = 0; i < models.Count; i++) {
			rt = new RenderTexture (256, 256, 16, RenderTextureFormat.ARGB4444);
			rt.Create ();
			rt.name = "RenderTexture" + models [i];
			cam.targetTexture = rt;

			_preview = Utils.GetInstance ().Get ("GUI/ScrollPreview");
			_preview.transform.position = new Vector2 (160 * i, 0);
			_preview.transform.SetParent (rect.content, false);


			btn = _preview.GetComponent<Button> ();
			int local_i = i;
			btn.onClick.AddListener (delegate {
				SelectModel (local_i);
			}
			);


			raw = _preview.GetComponentInChildren<RawImage> ();
			raw.texture = cam.targetTexture;

			model = LoadModel (models [i]);
			model.transform.SetParent (previewCont.transform, false);

			cam.Render ();

			model.SetActive (false);
		}
//
		rt = new RenderTexture (256, 256, 16, RenderTextureFormat.ARGB4444);
		rt.Create ();

		renderTexture.GetComponent<RawImage> ().texture = rt;

		model = LoadModel (models [0]);
		model.transform.SetParent (previewCont.transform, false);

		ani = model.GetComponent<Animation> ();

		cam.targetTexture = rt;
		cam.Render ();

	}

	public void SelectModel (int value)
	{
		tempModelIndex = value;

		skinName.text = models [tempModelIndex] + (tempSkinIndex + 1);
		ChangeModel ();
	}

	public void SelectSkin (int value)
	{
		tempSkinIndex = value;
		skinName.text = models [tempModelIndex] + (tempSkinIndex + 1);
		ChangeModel ();
	}

	void ChangeModel ()
	{
		model.SetActive (false);
		model = LoadModel (models [tempModelIndex]);
		model.transform.SetParent (previewCont.transform, false);
	}

	public void Submit ()
	{
		Utils.currentModelName = models [tempModelIndex];
		Utils.currentSkinIndex = tempSkinIndex;

		GameEvent.SelectPlayerModel ();
	}

	GameObject LoadModel (string value)
	{
		return  Utils.GetInstance ().Get ("Player/" + value + "/" + value + (tempSkinIndex + 1));
	}

	void  Update ()
	{
		idleTime -= Time.deltaTime;
		if (idleTime < 0) {
			int rnd = Random.Range (1, 3);
			ani.Play ("Shop" + tempModelName + rnd);
			idleTime = 5;
		}
		if (!ani.isPlaying) {
			ani.CrossFade ("Shop" + tempModelName + 1);
		}
//		model.transform.Rotate (Vector3.up * 2);
		cam.Render ();
	}

	void OnEnable ()
	{
		ChangeModel ();
	}

	void OnDisable ()
	{
		if (ani != null)
			ani.Stop ();
	}
}