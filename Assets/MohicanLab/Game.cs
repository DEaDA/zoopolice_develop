﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine.CrashLog;
//using Facebook.Unity;
//using Facebook.Unity.Canvas;
using UnityEngine.UI;
public class Game : MonoBehaviour
{
	public GameObject gameCamera;
	[SerializeField]
	GameModel model;
	[SerializeField]
	GameView view;
	[SerializeField]
	public Text log;
	[SerializeField]
	public GameController controller;

//	FogSettings fog;

	void Awake ()
	{
//		CrashReporting.Init ("54c9562a-85b1-4f28-82ef-4f605076902b", "0.0.1");
		DOTween.Init ();

//		QualitySettings.vSyncCount = 0;  
//		Application.targetFrameRate = 60;

//		fog =
		gameObject.AddComponent<FogSettings> ();

	}

	void Start ()
	{
		model.Init ();
		controller.Init (model);
		view.Init (model, controller);

		model.UpdateGame ();
	}
}
