﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using Facebook;
using Facebook.Unity;
using Facebook.MiniJSON;

public class GUI : MonoBehaviour
{
//	GameModel model;

	public Text coins;
	public Text score;
	public Text highScore;
	//	public TextMesh info;

	//btns
	public GameObject btnRestart;
	public GameObject btnNewGame;
	public GameObject btnCar;

	//screens
	public GameObject mainScreen;
	public GameObject endScreen;
	public GameObject gameScreen;
	public GameObject NewScoreScreen;
	public Text NewScoreText;
	Button btn3;

	int currentScore = 0;

	public void Awake ()
	{
		GameEvent.onGameUpdate += UpdateUI;
		GameEvent.onPlayerDied += GameEvent_onPlayerDied;
//		GameEvent.onOpenCage += HideMainScreen;

		endScreen.SetActive (false);

		Button btn = btnNewGame.AddComponent<Button> ();
		btn.onClick.AddListener (() => {
			onClickNewGame ();
		}
		);

		Button btn2 = btnRestart.GetComponent<Button> ();
		btn2.onClick.AddListener (() => {
			onClickRestart ();
		}
		);

		btn3 = NewScoreScreen.AddComponent<Button> ();
		btn3.onClick.AddListener (() => {
			onCloseNewScore ();
		}
		);
	
	}

	void onCloseNewScore ()
	{
		NewScoreScreen.GetComponent<NewHighScoreWindow> ().Close ();

		GameEvent.GameOver ();
		mainScreen.SetActive (true);
		btnNewGame.SetActive (true);
		endScreen.SetActive (false);
		gameScreen.SetActive (false);
	}

	void onUseCar ()
	{
		GameEvent.UseCar ();
	}

	void Start ()
	{
		if (PlayerPrefs.HasKey (SaveData.COINS)) {
			coins.text = string.Format ("Coins: {0}", PlayerPrefs.GetInt (SaveData.COINS));
		} else {
			coins.text = string.Format ("Coins: {0}", 0);
		}

		if (PlayerPrefs.HasKey (SaveData.SCORE)) {
			highScore.text = string.Format ("Score: {0}", PlayerPrefs.GetInt (SaveData.SCORE));
		} 


//		info.text = SystemInfo.deviceModel;
//		info.text += "\n" + SystemInfo.deviceName;
//		info.text += "\n" + SystemInfo.deviceType;
//		info.text += "\n" + SystemInfo.deviceUniqueIdentifier;
//		info.text += "\n" + SystemInfo.operatingSystem;
	}

	public void onClickRestart ()
	{

		endScreen.SetActive (false);
		gameScreen.SetActive (false);

		if (currentScore>PlayerPrefs.GetInt (SaveData.SCORE)) {
			PlayerPrefs.SetInt (SaveData.SCORE, currentScore);
			highScore.text = "" + currentScore;
			NewScoreText.text = "" + currentScore;
			mainScreen.SetActive (false);
			NewScoreScreen.GetComponent<NewHighScoreWindow> ().Open ();
			PlayerPrefs.Save ();
		} else {
			GameEvent.GameOver ();
			mainScreen.SetActive (true);
			btnNewGame.SetActive (true);
		}
	}

	public void onClickNewGame ()
	{
		currentScore = 0;
		score.text = "Score: 0";

		btnNewGame.SetActive (false);
		GameEvent.OpenCage ();
		mainScreen.SetActive (false);
		gameScreen.SetActive (true);
	}

	void GameEvent_onPlayerDied ()
	{
		endScreen.SetActive (true);
	}

	public void UpdateUI (GameModel model)
	{
//		this.model = model;
		coins.text = string.Format ("Coins: {0}", model.coins);
		score.text = string.Format ("Score: {0}", model.score);
		currentScore = model.score;
	}
}
