﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CameraFollow : MonoBehaviour
{

	readonly Vector3 DEFAULT_POSITION = new Vector3 (0, 0.5f, 0.6f);


	readonly float DEFAULT_ANGLE = 25;//30
	readonly float DEFAULT_DISTANCE = -0.1f;//-0.25f;
	readonly float DEFAULT_HEIGHT = 0.5f;

	public Transform target;
	public float angle = 0;
	public float distance = 0;
	public float height = 0;

	private Vector3 posCamera;
	private Vector3 angleCam;
	private bool shake;
	bool fly = false;
	float rotateX = 0;
	public bool died = false;

	void Start ()
	{
		GameEvent.onPlayerDied += FocusOnPlayer;
		GameEvent.onPlayerFly += onPlayerFly;
		GameEvent.onPlayerStopFly += onPlayerStopFly;

		Reset ();
	}

	void onPlayerStopFly ()
	{
		fly = false;
	}

	void onPlayerFly ()
	{
		fly = true;
	}

	bool begin = false;

	public void StopBegin ()
	{
		begin = !true;
	}

	public void Begin ()
	{
		begin = true;
	}

	public void FocusOnPlayer ()
	{
		died = true;
		angle = 50;
		distance = -0.3f;
		deadZ = 0.75f;
	}

	float deadZ = 0;


	[SerializeField]
	public float flyPosY = 3.1f;

	void LateUpdate ()
	{

		float speedScale = 1; 
		if (died)
			speedScale = 2;
		if (target != null) {
			if (target.position.z >= 0) {
				if (!begin) {
					posCamera.x = Mathf.Lerp (posCamera.x, target.position.x, 10 * Time.deltaTime / speedScale);
					posCamera.y = transform.position.y;
					posCamera.z = transform.position.z;
				}
				if (died)
					posCamera.z = deadZ;

				if (!begin)
					posCamera.y = Mathf.Lerp (posCamera.y, target.position.y + height, 4 / speedScale);
				angleCam.x = angle + rotateX;

				if (fly) {
					posCamera.y = flyPosY;
					angleCam.x = 10f;
				}
				transform.position = posCamera;

			
//				angleCam.y = Mathf.Lerp (angleCam.y, 0, 1 * Time.deltaTime);
//				angleCam.z = transform.eulerAngles.z;
				if (!begin)
					transform.eulerAngles = Vector3.Lerp (transform.eulerAngles, angleCam, 10 * Time.deltaTime / speedScale);
			}
		}
	}

	public void Reset ()
	{
		deadZ = 0;
		angle = DEFAULT_ANGLE;
		distance = DEFAULT_DISTANCE;
		height = DEFAULT_HEIGHT; 
		died = false;

	

		transform.position = new Vector3 (DEFAULT_POSITION.x, DEFAULT_POSITION.y, DEFAULT_POSITION.z);
	}
}
