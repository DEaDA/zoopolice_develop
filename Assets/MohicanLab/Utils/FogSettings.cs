﻿using UnityEngine;
using System.Collections;
using VacuumShaders.CurvedWorld;

public class FogSettings : MonoBehaviour
{
	CurvedWorld_Controller c;

	//	bool down = true;
	bool right = true;
	float value = 50;
	//	int value2 = 50;

	bool run = false;

	void Start ()
	{
		GameEvent.onBeginRun += BeginUpdate;
		GameEvent.onPlayerDied += StopUpdate;

		c = gameObject.AddComponent<CurvedWorld_Controller> ();
		c._V_CW_Bend_X = -40;
		c._V_CW_Bias_X = 0;
		c._V_CW_Bend_Y = 50;
		c._V_CW_Bias_Y = 2;

		RenderSettings.fogMode = FogMode.Linear;
		RenderSettings.fogStartDistance = 3;//fogStartDistance;
		RenderSettings.fogEndDistance = 5;//fogEndDistance;
		RenderSettings.fogColor = ToColor (0xABC9D3);// (0xD2D7B3);//(0x9bffff);//(0x447C9A); 
		RenderSettings.fog = fog;
	}

	//	[SerializeField]
	//	public Color fogColor = Color.white;// 0x447C9A;
	//	[SerializeField]
	public bool fog = true;
	//	[SerializeField]
	public float fogStartDistance = 5;
//4;//4;
	//	[SerializeField]
	public float fogEndDistance = 7;
//9;//16

	public Color32 ToColor (int HexVal)
	{
		byte R = (byte)((HexVal >> 16) & 0xFF);
		byte G = (byte)((HexVal >> 8) & 0xFF);
		byte B = (byte)((HexVal) & 0xFF);
		return new Color32 (R, G, B, 255);
	}

	void FixedUpdate ()
	{
//		RenderSettings.fogMode = FogMode.Linear;
		RenderSettings.fogStartDistance = fogStartDistance;
		RenderSettings.fogEndDistance = fogEndDistance;
//		RenderSettings.fogColor = fogColor;// ToColor (0x447C9A); 
		//		RenderSettings.fog = fog;5;/
	
		if (!run)
			return;

		if (value >= 50)
			right = true;

		if (value <= -50)
			right = false;

		if (right)
			value -= 0.1f;
		else
			value += 0.1f;
	
		c._V_CW_Bend_Y = value;

//		if (value2 == 50)
//			down = true;
//
//		if (value2 == -50)
//			down = false;
//
//		if (down)
//			value2--;
//		else
//			value2++;
//
//		c._V_CW_Bias_X = value2;
	}

	void BeginUpdate ()
	{
		run = true;
	}

	void StopUpdate ()
	{
		run = false;
	}
}
