﻿// Author:
//       DEaDA <deada@deada.net>
// Copyright (c) 2016 Konstantin Arbuzov
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

public class Utils
{
	public static  readonly string KENG = "Kangooro";
	public static  readonly string CROC = "Crocodile";
	public static  readonly string PAND = "Panda";

	public static string currentModelName = KENG;
	public static int currentSkinIndex = 0;
	public static int currentScooterIndex = 0;


	public static readonly string ZOO_GATE = "Loc1/ZooGate";
	public static readonly string ROAD = "Road/Road";
	public static readonly string COIN = "Items/Coin";
	public static readonly string BOOSTER = "Items/Booster";

	public static readonly string BLOCK_SHORT = "Blocks/BlockShort";
	public static readonly string BLOCK_HIGH = "Blocks/BlockHigh";
	public static readonly string BLOCK_UNDER = "Blocks/BlockUnder";
	public static readonly string BLOCK_UNDER2 = "Blocks/BlockUnder2";

	public static readonly string BLOCK_LONG1 = "Cars/BlockLong1";
	public static readonly string BLOCK_LONG2 = "Cars/BlockLong2";
	public static readonly string BLOCK_LONG3 = "Cars/BlockLong3";
	public static readonly string BLOCK_LONG4 = "Cars/BlockLong4";
	public static readonly string BLOCK_LONG5 = "Cars/BlockLong5";
	public static readonly string BLOCK_LONG6 = "Cars/BlockLong6";

	public static readonly string BLOCK_RAMP1 = "Cars/Ramp1";
	public static readonly string BLOCK_RAMP2 = "Cars/Ramp2";

	Dictionary<string, List<GameObject>> pools = new Dictionary<string, List<GameObject>> ();
	private static Utils instance = null;
	Dictionary<string, GameObject> gameObjects;

	public  GameObject LoadCurrentModel ()
	{
		Trace.Log (KENG, "Player/" + currentModelName + "/" + currentModelName + (currentSkinIndex + 1));
		return Get ("Player/" + currentModelName + "/" + currentModelName + (currentSkinIndex + 1));
	}

	public GameObject LoadCurrentScooter ()
	{
		return Get ("Scooters/Scooter" + (currentScooterIndex + 1));
	}

	public GameObject GetPrefab (string name)
	{
		if (gameObjects == null)
			gameObjects = new Dictionary<string, GameObject> ();
		if (gameObjects.ContainsKey (name))
			return gameObjects [name];
		string path = name;
		GameObject go = Resources.Load (path) as GameObject;
		gameObjects [name] = go;
		return go;
	}

	List<GameObject> GetPool (string name)
	{
		if (!pools.ContainsKey (name))
			pools [name] = new List<GameObject> ();
		return pools [name];
	}

	public GameObject Get (string name)
	{
		GameObject go = null;
		List<GameObject> pool = GetPool (name);
		int len = pool.Count;
		for (int i = 0; i < len; i++) {
			if (!pool [i].activeSelf) {
				go = pool [i];
				break;
			}
		}
		if (go == null) {
			go = Instantiate (name);
			pool.Add (go);
		}
		go.SetActive (true);
		return go;
	}

	GameObject Instantiate (string name)
	{
		return MonoBehaviour.Instantiate (GetPrefab (name), Vector3.zero, Quaternion.identity) as GameObject;
	}

	public GameObject Create (string name)
	{
		if (gameObjects == null) {
			gameObjects = new Dictionary<string, GameObject> ();
		}
		if (gameObjects.ContainsKey (name)) {
			return gameObjects [name];
		}
		string path = name;
		GameObject go = Resources.Load (path) as GameObject;
		gameObjects [name] = go;
		return go;
	}

	public  Bounds GetMaxBounds (GameObject obj)
	{
		Bounds b = new Bounds (obj.transform.position, Vector3.zero);
		Renderer[] arr = obj.GetComponentsInChildren<Renderer> ();
		int max = arr.Length;
		Renderer r;
		for (int i = 0; i < max; i++) {
			r = arr [i];
			b.Encapsulate (r.bounds);
		}
		return b;
	}

	public static Utils GetInstance ()
	{
		if (instance == null)
			instance = new Utils ();
		return instance;
	}
}

