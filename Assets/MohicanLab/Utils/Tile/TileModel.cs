﻿// Author:
//       DEaDA <deada@deada.net>
// Copyright (c) 2016 Konstantin Arbuzov
using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class TileModel
{
	public enum TYPE
	{
		EMPTY,
		BLOCK,
		BLOCK_ROLL,
		BLOCK_JUMP,
		BLOCK_RAMP,
		BLOCK_LONG
	}

	public Color color = Color.yellow;
	public List<TileModel> group;
	public bool block;
	public int x;
	public int z;
	public readonly int MAXGROUP = 3;
	TYPE currentType = TYPE.EMPTY;

	public void Clean ()
	{
		if (group != null) {
			group.Clear ();
			group = null;
		}
		block = false;
		currentType = TYPE.EMPTY;
	}

	public void Add (TileModel tile)
	{
		if (group == null)
			group = new List<TileModel> ();
		if (group.Count <= MAXGROUP)
			group.Add (tile);
		else
			MonoBehaviour.print ("Cannot add a new tile to the group.");
	}

	public TYPE type {
		get { return currentType; }
		set{ currentType = value; }
	}
}

