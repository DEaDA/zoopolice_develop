﻿using UnityEngine;
using System.Collections;

using System; 
using System.Reflection;

public class Trace
{
	public static void Log (params object[] a)
	{
//		#if UNITY_EDITOR
		var s = a [0].ToString ();
		for (int i = 1; i < a.Length; i++) {
			s += " ";
			if (a [i] != null)
				s += a [i].ToString ();
			else
				s += "null";
		}
		Debug.Log (s);
//		#endif
	}

	public static void LogClear()
	{
		#if UNITY_EDITOR
//		var assembly = Assembly.GetAssembly(typeof(UnityEditor.ActiveEditorTracker));
//		var type = assembly.GetType("UnityEditorInternal.LogEntries");
//		var method = type.GetMethod("Clear");
//		method.Invoke(new object(), null);
		#endif
	}
}
