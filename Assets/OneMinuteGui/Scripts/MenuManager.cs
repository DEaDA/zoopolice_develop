﻿using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
	[SerializeField]
	private string m_animationPropertyName;

	[SerializeField]
	private GameObject m_initialScreen;

	[SerializeField]
	private List<GameObject> windows;

	[SerializeField]
	public GameObject renderCam;

	GameObject shop;

	public void GoBack ()
	{
		if (windows.Count > 1) 
			Animate (windows [windows.Count - 1], true);
	}

	public void GoToMenu (GameObject target)
	{
		Animate (target, true);
	}

	private void Animate (GameObject target, bool close)
	{
		if (close) {
			int idx = windows.Count - 1;
			GameObject prev = windows [idx];
			windows.RemoveAt (idx);
			prev.SetActive (false);
		}

		if (target == null) {
			return;
		}

		target.SetActive (true);
		windows.Add (target);

		Canvas canvasComponent = target.GetComponent<Canvas> ();
		if (canvasComponent != null) {
			canvasComponent.overrideSorting = true;
			canvasComponent.sortingOrder = windows.Count;
		}
	}

	private void Awake ()
	{
		windows = new List<GameObject>{ m_initialScreen };
	}
}
